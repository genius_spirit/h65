import React, { Component } from 'react';
import Layout from "./Components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import About from "./Containers/About/About";
import Admin from "./Containers/Admin/Admin";


class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={About}/>
          <Route path="/pages/:category" component={About}/>
          <Route path="/admin" exact component={Admin}/>
          <Route render={() => <h1>Not found</h1>}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
