import React from 'react';
import './Toolbar.css';
import Menu from "../Menu/Menu";

const Toolbar = () => {
  return(
    <div className="Toolbar">
      <div className="Logo">Сила ума</div>
      <nav>
        <Menu />
      </nav>
    </div>
  )
};

export default Toolbar;