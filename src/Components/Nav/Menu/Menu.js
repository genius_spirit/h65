import React from 'react';
import './Menu.css';
import {NavLink} from "react-router-dom";

const Menu = () => {
  return(
    <div className="Menu">
      <ul className="Menu-list">
        <li><NavLink to="/">Домой</NavLink></li>
        <li><NavLink to="/pages/love" >О любви</NavLink></li>
        <li><NavLink to="/pages/live" >О жизни</NavLink></li>
        <li><NavLink to="/pages/vine" >О вине</NavLink></li>
        <li><NavLink to="/admin">Admin</NavLink></li>
      </ul>
    </div>
  )
};

export default Menu;