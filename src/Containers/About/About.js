import React, { Component } from 'react';
import axios from 'axios';
import Spinner from "../../Components/Spinner/Spinner";

class About extends Component {

  state = {
    data: [],
    loading: false
  };

  getPageInfo = () => {
    this.setState({loading: true});
    axios.get('/about.json').then(response => {
      const pageInfo = ({id: new Date(), title: response.data.title, text: response.data.text});
      this.setState({data: pageInfo});
    }).then(() => this.setState({loading: false}));
  };

  componentDidMount() {
    this.getPageInfo();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.category !== prevProps.match.params.category) {
      this.setState({loading: true});
      const category = this.props.match.params.category;
      if (category) {
        axios.get(`/${category}.json`).then(response => {
          const pageInfo = ({id: new Date(), title: response.data.title, text: response.data.text});
          this.setState({data: pageInfo});
        }).then(() => this.setState({loading: false}));
      } else {
        this.getPageInfo();
      }
    }
  }

  render() {
    let page =
      <div className="About-container">
        <h2>{this.state.data.title}</h2>
        <p>{this.state.data.text}</p>
      </div>;

    if (this.state.loading) {
      page = <Spinner />;
    }
    return page;
  }

}

export default About;