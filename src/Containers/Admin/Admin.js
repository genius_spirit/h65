import React, {Component} from 'react';
import {Button, FormControl, Input, InputLabel, MenuItem, Select, TextField} from "material-ui";
import './Admin.css';
import axios from "axios/index";
import Spinner from "../../Components/Spinner/Spinner";

class Admin extends Component {

  state = {
    category: '',
    title: '',
    text: '',
    loading: false
  };

  changeHandler = event => this.setState({[event.target.name]: event.target.value });

  handleChange = event => this.setState({category: event.target.value});

  editCategoryHandler = () => {
    this.setState({loading: true});
    let object = {title: this.state.title, text: this.state.text};
    axios.patch(`/${this.state.category}.json`, object).then(() => {
      this.setState({loading: false});
      this.props.history.push(`/pages/${this.state.category}`);
    });
  };

  componentDidUpdate(props, prevState) {
    if (prevState.category !== this.state.category) {
      this.setState({loading: true});
      axios.get(`/${this.state.category}.json`).then(response => {
        this.setState({title: response.data.title, text: response.data.text});
      }).then(() => this.setState({loading: false}));
    }
  }

  render() {
    let form = (
      <div className="Admin-container">
        <h2>Редактирование страницы</h2>
        <div className="select-form">
          <FormControl>
            <InputLabel>Категория</InputLabel>
            <Select value={this.state.category} onChange={this.handleChange} className="Admin-select">
              <MenuItem value=""><em>Пусто</em></MenuItem>
              <MenuItem value="love">Омар Хайям о любви</MenuItem>
              <MenuItem value="live">Омар Хайям о жизни</MenuItem>
              <MenuItem value="vine">Омар Хайям о вине</MenuItem>
            </Select>
          </FormControl>
        </div>
        <div className="input-form">
          <FormControl>
            <Input className="Admin-input" name="title"
                   value={this.state.title} onChange={this.changeHandler} />
          </FormControl>
        </div>
        <div className="text-form">
          <TextField className="Admin-text" multiline rows="10" name="text"
                     value={this.state.text} onChange={this.changeHandler}/>
        </div>
        <Button variant="raised" className="btn" onClick={this.editCategoryHandler}>Save</Button>
      </div>
    );
    if (this.state.loading) {
      form = <Spinner/>;
    }
    return form;
  }
}

export default Admin;